#pragma once

#include <cstddef>

class Scan {
public:
    Scan();
    ~Scan();

    /** Compute the inclusive max.

        For example, for an input array
            {5, 3, 5, 1, 8, 0, 2}
        compute
            {5, 5, 5, 5, 8, 8, 8}.
    */
    void inclusiveMax(const double *inDev, double *outDev, int N);

private:
    // TODO: Put here pointers to any temporary buffers you might need, such
    // that no allocation occurs if the `inclusiveMax` function is invoked
    // multiple times with the same `N` (apart from the first run, of course).
    // In practice, avoiding allocation/deallocation all the time is desirable
    // because cudaFree must synchronize the device!
};
