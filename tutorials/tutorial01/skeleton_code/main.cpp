#include <iostream>
#include <vector>
#include <mpi.h>
#include <omp.h>

void matrix_multiply(const std::vector<double>& A, const std::vector<double>& B,
                     std::vector<double>& C, int size)
{
    // TODO: parallelize with openMP

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            double sum = 0.0;
            for (int k = 0; k < size; k++) {
                sum += A[i*size + k] * B[k*size + j];
            }
            C[i*size + j] = sum;
        }
    }
}

int main(int argc, char** argv)
{
    int rank, num_procs;
    // TODO: initialize
    

    // TODO: make this output thread safe and workable for number of openMP threads!
    std::cout << "Hello! I am MPI rank[" << rank << "] with omp thread[" << omp_get_thread_num() << "]\n"; 
    


    int size = 100;

    // Allocate matrices A and B on each process
    std::vector<double> A(size * size);
    std::vector<double> B(size * size);
    std::vector<double> C(size * size);

    // Initialize matrices A and B on each process
    // TODO: parallelize with openMP

    for (int i = 0; i < size * size; i++) {
        //A[i] = 1.0 + rank;
        //B[i] = 1.0 + rank;

        A[i] = 1.0/(i + rank);
        B[i] = 1.0/(i + rank);
    }

    if(rank >= 0 && size < 4){
        std::cout << "Before matrix multiplication:\n";

        std::cout << "A:\n";
        for (int i = 0; i < size * size; i++) {
            if(i % size == 0){
                std::cout << "\n";
            }
            std::cout << A[i] << " ";
        }
        std::cout << "\n";

        std::cout << "B:\n";
        for (int i = 0; i < size * size; i++) {
            if(i % size == 0){
                std::cout << "\n";
            }
            std::cout << B[i] << " ";
        }
        std::cout << "\n";

        std::cout << "C:\n";
        for (int i = 0; i < size * size; i++) {
            if(i % size == 0){
                std::cout << "\n";
            }
            std::cout << C[i] << " ";
        }
        std::cout << "\n";
    }

    // Perform matrix multiplication on each process
    const int reps = 10;

    double time = MPI_Wtime();
    for(int i = 0; i < reps; i++){
        matrix_multiply(A, B, C, size);
    }
    time = MPI_Wtime() - time;

    // TODO: Allreduce the results


    if(size < 4){
        std::cout << "C:\n";
        for (int i = 0; i < size * size; i++) {
            if(i % size == 0){
                std::cout << "\n";
            }
            std::cout << C[i] << " ";
        }
        std::cout << "\n";
    }

    if(rank == 0){
        std::cout << "Execution time: " << time/double(reps) << " sec\n";
    }

    // TODO: finalize
    
    
    return 0;
}
