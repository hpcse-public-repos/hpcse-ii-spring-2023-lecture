\begin{question}{Matrix-vector multiplication with CUDA \examP{20}}

Let $\mathbf{M}$ be a \textbf{column-major} $N \times N$ matrix and $\mathbf{v}$ a vector of size $N$.
The task is to compute the matrix-vector product $\mathbf{w} = \mathbf{M} \mathbf{v}$.
A simple (and inefficient) C++ code for computing the product is as follows:
\begin{lstlisting}[basicstyle=\changefont{pcr}{m}{n}\footnotesize, style=cppCode, gobble=2]
  void gemvCPU(const double *m, const double *v, double *w, int N) {
    // Note: m is stored in column-major order!

    for (int i = 0; i < N; ++i) {
      double sum = 0.0;
      for (int j = 0; j < N; ++j)
        sum += m[j * N + i] * v[j];
      w[i] = sum;
    }
  }
\end{lstlisting}

\begin{subquestion}
  (10 points) Translate the function \verb|gemvCPU| to CUDA, such that each thread computes one element of $\mathbf{w}$.
  Make sure there are no out-of-bounds array accesses.

  \begin{solution}
    The solution code is the following:
    \begin{lstlisting}[basicstyle=\changefont{pcr}{m}{n}\footnotesize, style=cppCode, gobble=6]
      __global__ void gemvGPU1(const double *m, const double *v, double *w, int N) {
        int idx = blockIdx.x * blockDim.x + threadIdx.x;
        if (idx < N) {
          double sum = 0.0;
          for (int j = 0; j < N; ++j)
            sum += m[j * N + idx] * v[j];
          w[idx] = sum;
        }
      }

      ...
      const int kBlockSize = 128;
      gemvGPU1<<<(N + kBlockSize - 1) / kBlockSize, kBlockSize>>>(mDev, vDev, wDev, N);
      ...
    \end{lstlisting}
    The transformation from the naive CPU to the naive GPU code is performed by replacing the outer for loop \verb|i| with the thread parallelism.
    We use $N$ threads (or slightly more, if $N$ is not divisible by the number of threads per block), one for each iteration of the \verb|i| for loop.
    The body (summation and computation of \verb|w[i]|) is copy-pasted as is, only replacing the variable \verb|i| with the grid-level thread index \verb|idx|.
    One must only make sure to ignore the threads whose \verb|idx| is out of the bounds of the array (hence the \verb|if (idx < N) { ... }|).
  \end{solution}
\end{subquestion}

\begin{subquestion}
   (10 points) In a naive implementation, each thread of each block reads the whole vector $\mathbf{v}$ from memory.
  Optimize the code by using shared memory.
  That is, ensure that elements of $\mathbf{v}$ are read from the global memory only once per block, as opposed to once per thread.

  \begin{solution}
    The goal is to optimize the inner for loop \verb|j| and split it into chunks of size \verb|kBlockSize|, using the following logic:

    \begin{lstlisting}[basicstyle=\changefont{pcr}{m}{n}\footnotesize, style=cppCode, gobble=6]
      // for (int j = 0; j < N; ++j) { ... }
      for (int offset = 0; offset < N; offset += kBlockSize) {
        // 1. Copy a[offset], a[offset + 1], ..., a[i + kBlockSize] to shared memory.
        // 2. Synchronize threads.
        // 3. Use the temporary shared buffer to compute the product.
        for (int j = 0; j < kBlockSize; ++j) {
          sum += ...;
        }
        // 4. Synchronize threads again.
      }
    \end{lstlisting}

    Refer to the solution code for details.
  \end{solution}
\end{subquestion}

\vspace{1ex}
\textbf{Note:} no error checking is necessary.

\end{question}
