import json
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
from scipy.stats import norm
import scipy.optimize as optimize


def get_quantiles(x_, samples, q):

    a = samples[:,0]
    t = samples[:,1]
    w = samples[:,2]
    b = samples[:,3]
    sigma = samples[:,4]

    a_ = np.mean(a)
    t_ = np.mean(t)
    w_ = np.mean(w)
    b_ = np.mean(b)

    y_ = []
    for x in x_:
        def func(y):
            cdf = np.mean(norm.cdf(y,
                                   loc=a * x + b * np.tanh((x-t)/w),
                                   scale=sigma))
            return cdf - q

        res = optimize.fsolve(func, x0=[a_ * x + b_ * np.tanh((x-t_)/w_)])
        y_.append(res[0])

    return np.array(y_)



if __name__ == '__main__':

    with open(os.path.join("_korali_result", "latest"), "r") as f:
        j = json.load(f)

    samples = np.array(j["Results"]["Posterior Sample Database"])

    df = pd.read_csv('data.csv')
    x = df["x"].to_numpy()
    y = df["y"].to_numpy()


    x_ = np.linspace(x[0]-1, x[-1]+1, 200)

    y_median = get_quantiles(x_, samples, 0.5)
    y_25 = get_quantiles(x_, samples, 0.25)
    y_75 = get_quantiles(x_, samples, 0.75)
    y_05 = get_quantiles(x_, samples, 0.05)
    y_95 = get_quantiles(x_, samples, 0.95)

    fig, ax = plt.subplots()

    ax.plot(x_, y_median, '-r', label="median")
    ax.fill_between(x_, y_05, y_95, lw=0, color=(255/255, 214/255, 213/255), label="90% confidence")
    ax.fill_between(x_, y_25, y_75, lw=0, color=(255/255, 180/255, 178/255), label="50% confidence")
    ax.plot(x, y, 'ok', label="data")
    ax.set_xlim(x_[0], x_[-1])
    ax.set_xlabel(r"$x$")
    ax.set_ylabel(r"$y$")
    ax.legend()
    plt.savefig("predictions.pdf", transparent=True)
    plt.close()


    x0 = 5
    y_001 = get_quantiles([x0], samples, 0.001)[0]
    y_999 = get_quantiles([x0], samples, 0.999)[0]

    y_ = np.linspace(y_001, y_999, 200)

    a = samples[:,0]
    t = samples[:,1]
    w = samples[:,2]
    b = samples[:,3]
    sigma = samples[:,4]

    cdf = np.mean(norm.cdf(y_[:,np.newaxis],
                           loc=a[np.newaxis,:] * x0 + b[np.newaxis,:] * np.tanh((x0-t[np.newaxis,:])/w[np.newaxis,:]),
                           scale=sigma[np.newaxis,:]),
                  axis=1)

    q = 0.75
    y_75 = get_quantiles([x0], samples, q)[0]


    fig, ax = plt.subplots()
    ax.plot(y_, cdf)
    ax.plot([y_[0], y_75, y_75], [q, q, 0], '--k')
    ax.set_xlabel(r"$y$")
    ax.set_ylabel(r"$C(y|D;x)$")
    ax.set_xlim(y_[0], y_[-1])
    ax.set_ylim(0,1)
    plt.savefig("cdf.pdf", transparent=True)

    # empirical approach
    num_samples = len(samples)
    K = 20000
    y = []
    for k in range(K):
        i = np.random.randint(num_samples)
        a, t, w, b, sigma = samples[i,:]
        y.append(np.random.normal(loc=a*x0 + b*np.tanh((x0-t)/w), scale=sigma))

    print(y_75)
    print(np.quantile(y, q=0.75))
