import numpy as np
import pandas as pd

np.random.seed(123456)

n = 30
a = 0.3
t = 1.2
w = 0.5
b = 5
sigma = 0.6
x = np.linspace(-10, 10, n)
y = np.random.normal(loc=a * x + b * np.tanh((x-t)/w),
                     scale=sigma)

df = pd.DataFrame({"x": x, "y": y})
df.to_csv("data.csv", index=False)

import matplotlib.pyplot as plt
fig, ax = plt.subplots()
ax.plot(x, y, 'ok')
ax.set_xlabel(r"$x$")
ax.set_ylabel(r"$y$")
plt.savefig("data.pdf", transparent=True)
