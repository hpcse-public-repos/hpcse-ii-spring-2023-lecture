import korali
import numpy as np
import pandas as pd

if __name__ == '__main__':

    df = pd.read_csv('data.csv')
    x = df["x"].to_numpy()
    y = df["y"].to_numpy()
    n = len(x)

    def log_likelihood(ks):
        a, t, w, b, sigma = ks["Parameters"]
        log_like = \
            - n/2 * np.log(2*np.pi) \
            - n * np.log(sigma) \
            - np.sum((a * x + b * np.tanh((x-t)/w) - y)**2) / (2 * sigma**2)
        ks["logLikelihood"] = log_like


    e = korali.Experiment()

    e["Problem"]["Type"] = "Bayesian/Custom"
    e["Problem"]["Likelihood Model"] = log_likelihood

    e["Solver"]["Type"] = "Sampler/TMCMC"
    e["Solver"]["Population Size"] = 500
    e["Solver"]["Target Coefficient Of Variation"] = 1.0
    e["Solver"]["Covariance Scaling"] = 0.2

    e["Distributions"] = [
        {"Name": "Prior a",
         "Type": "Univariate/Uniform",
         "Minimum": -2.0,
         "Maximum": +2.0},

        {"Name": "Prior t",
         "Type": "Univariate/Uniform",
         "Minimum": -2.0,
         "Maximum": +2.0},

        {"Name": "Prior w",
         "Type": "Univariate/Uniform",
         "Minimum": 0.01,
         "Maximum": 2.00},

        {"Name": "Prior b",
         "Type": "Univariate/Uniform",
         "Minimum": 0.0,
         "Maximum": 10.0},

        {"Name": "Prior sigma",
         "Type": "Univariate/Uniform",
         "Minimum": 0.01,
         "Maximum": 2.00}
    ]


    e["Variables"] = [
        {"Name": "a", "Prior Distribution": "Prior a"},
        {"Name": "t", "Prior Distribution": "Prior t"},
        {"Name": "w", "Prior Distribution": "Prior w"},
        {"Name": "b", "Prior Distribution": "Prior b"},
        {"Name": "sigma", "Prior Distribution": "Prior sigma"}
    ]

    e["Store Sample Information"] = True

    e["Console Output"]["Verbosity"] = "Detailed"
    k = korali.Engine()
    k.run(e)
