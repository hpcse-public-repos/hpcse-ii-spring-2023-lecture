#include <mpi.h>
#include <omp.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 This function measures how accurate the C compared to the global C_gl we are.
*/
void verifyMatMulResults(double* C_gl, double* C, const int n, const int N, const int rankx, const int ranky, double execTime){
  double tolerance = 1e-6;
  int error = 0;
  for(int i = 0; i < n; ++i){
    for(int j = 0; j < n; ++j){
      error += fabs(C[i*n + j] - C_gl[i*N + j + ranky*n + rankx*n*N]) > tolerance;
    }
  }
  int tempErr = error;
  MPI_Reduce(&tempErr, &error, 1, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);

  if (rankx == 0 && ranky == 0)
  {
    if (error) { printf("\n[Error] Verification Failed!\n");  exit(-1); }

    double gflops = (((2e-9)*N)*N)*N/execTime;
    printf("Passed! \n");
    printf("Execution time: %.3fs \n",execTime);
    printf("GFlop/s: %.4f \n",gflops);
  }
}

/*
This function prints matrix A, B & C out
*/
void print_matrices(double* A, double* B, double* C, int size){
  std::cout << "A:\n";
  for (int i = 0; i < size * size; i++) {
      if(i % size == 0){
          std::cout << "\n";
      }
      std::cout << A[i] << " ";
  }
  std::cout << "\n";

  std::cout << "B:\n";
  for (int i = 0; i < size * size; i++) {
      if(i % size == 0){
          std::cout << "\n";
      }
      std::cout << B[i] << " ";
  }
  std::cout << "\n";

  std::cout << "C:\n";
  for (int i = 0; i < size * size; i++) {
      if(i % size == 0){
          std::cout << "\n";
      }
      std::cout << C[i] << " ";
  }
  std::cout << "\n";
}

/*
This function does the mmm for C += A x B (watch out it accumulates to C!)
*/
void matrix_multiply(double* A, double* B, double* C, int size){
  
  // TODO: parallelize with openMP

  for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
          double sum = 0.0;
          for (int k = 0; k < size; k++) {
              sum += A[i*size + k] * B[k*size + j];
          }
          C[i*size + j] += sum;
      }
  }
}

// BONUS: use Lapack instead of our matrix-matrix multiplication and see how much faster it is!
extern "C" void dgemm_(const char *transa, const char *transb, const int *m, const int *n, const int *k, const double *alpha, const double *a, const int *lda, const double *b, const int *ldb, const double *beta, double *c, const int *ldc);

int main(int argc, char* argv[])
{
  /**********************************************************
  *  Initial Setup
  **********************************************************/
  size_t N = 4;
  double *A,*B,*C, *tmpA, *tmpB;

  // TODO: MPI initialize here
  int rank, size;
  
 /**********************************************************
  *  Determining rank geometry < You need to improve this >
  **********************************************************/

  int p = sqrt(size);
  if (size != p*p) { printf("[Error] Number of processors must be a square integer.\n"); MPI_Abort(MPI_COMM_WORLD, -1); }

  // TODO: create own communicator in p*p 2D with periodic boundary conditions
  MPI_Comm cannonComm;

  // TODO: Initialize the next neighbors up, down, left, right
  int up, down, left, right;
  

  // TODO: Get the rankx & ranky coordinates
  int rankx;
  int ranky;

  #pragma omp parallel
  {
    #pragma omp critical
    std::cout << "Hello! I am MPI rank[" << rank << "] with: " << rankx << ranky << "," << up << down << left << right << " omp thread[" << omp_get_thread_num() << "]\n"; 
  }
  MPI_Barrier(MPI_COMM_WORLD);


 /********************************************
  *   Initializing Matrices
  *******************************************/

  // Determining local matrix side size (n)
  const int n = N/p;

  // Allocating space for local matrices A, B, C
  A    = (double*) calloc (n*n, sizeof(double));
  B    = (double*) calloc (n*n, sizeof(double));
  C    = (double*) calloc (n*n, sizeof(double));



  // Allocating space for recieve buffers for A and B local matrices
  tmpA = (double*) calloc (n*n, sizeof(double));
  tmpB = (double*) calloc (n*n, sizeof(double));

  // Initializing values for input matrices A and B


  double * A_gl = (double*) calloc (N*N, sizeof(double));
  double * B_gl = (double*) calloc (N*N, sizeof(double));
  double * C_gl = (double*) calloc (N*N, sizeof(double));
  for(int i = 0; i < N*N; ++i){
      A_gl[i] = 1.0/i;
  }
  for(int i = 0; i < N; ++i){
      for(int j = 0; j < N; ++j){
          B_gl[j*N + i] = 1.0/(i+j);
      }
  }

  // Control solution in C_gl
  matrix_multiply(A_gl, B_gl, C_gl, N);


  if(rank == 0 && N < 9){
    print_matrices(A_gl, B_gl, C_gl, N);
  }
  MPI_Barrier(MPI_COMM_WORLD);
  
  // TODO: Initialize matrix A & B accordingly in Cannon algorithm style wiki: https://en.wikipedia.org/wiki/Cannon%27s_algorithm
  for(int i = 0; i < n; ++i){
    for(int j = 0; j < n; ++j){

    }
  }

  /*
  if(rank == 1 && n < 5){
    print_matrices(A, B, C, n);
  }
  MPI_Barrier(MPI_COMM_WORLD);
  */
  
  
 /*******************************************************
  *  Creating Contiguous Datatype
  ******************************************************/

  // TODO: implement own matrix datatype here 

 /**************************************************************
  *   Running Cannon's Matrix-Matrix  Multiplication Algorithm
  **************************************************************/

  if (rank == 0) printf("Running Matrix-Matrix Multiplication...\n");

  // Registering initial time. It's important to precede this with a barrier to make sure
  // all ranks are ready to start when we take the time.
  MPI_Barrier(MPI_COMM_WORLD);
  double execTime = -MPI_Wtime();


  // TODO: Implement Cannon algorithm here (doen't worry about comm/comp overlapping)

  // one can do the first multiplication already here:
  matrix_multiply(A, B, C, n);
  for(int step = 0; step < p; step++)
  {
    // P2P communication here

    // use the tmpA & tmpB as the receiving buffer
    double* holdA= A; A = tmpA; tmpA = holdA;
    double* holdB= B; B = tmpB; tmpB = holdB;
  }

  // Registering final time. It's important to precede this with a barrier to make sure all ranks have finished before we take the time.
  MPI_Barrier(MPI_COMM_WORLD);
  execTime += MPI_Wtime();

 /**************************************************************
  *   Verification Stage
  **************************************************************/

  /*
  if(rank == 3 && n < 5){
    print_matrices(tmpA, tmpB, C, n);
  }
  MPI_Barrier(MPI_COMM_WORLD);
  */

  verifyMatMulResults(C_gl, C, n, N, rankx, ranky, execTime);

  // TODO: Finalize, free communicators and free memory here
  

  return 0;
}
