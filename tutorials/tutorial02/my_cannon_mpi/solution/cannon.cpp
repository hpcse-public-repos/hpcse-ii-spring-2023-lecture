#include <mpi.h>
#include <omp.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void verifyMatMulResults(double* C_gl, double* C, const int n, const int N, const int rankx, const int ranky, double execTime){
  double tolerance = 1e-6;
  int error = 0;
  for(int i = 0; i < n; ++i){
    for(int j = 0; j < n; ++j){
      error += fabs(C[i*n + j] - C_gl[i*N + j + ranky*n + rankx*n*N]) > tolerance;
    }
  }
  int tempErr = error;
  MPI_Reduce(&tempErr, &error, 1, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);

  if (rankx == 0 && ranky == 0)
  {
    if (error) { printf("\n[Error] Verification Failed!\n");  exit(-1); }

    double gflops = (((2e-9)*N)*N)*N/execTime;
    printf("Passed! \n");
    printf("Execution time: %.3fs \n",execTime);
    printf("GFlop/s: %.4f \n",gflops);
  }
}

void print_matrices(double* A, double* B, double* C, int size){
  std::cout << "A:\n";
  for (int i = 0; i < size * size; i++) {
      if(i % size == 0){
          std::cout << "\n";
      }
      std::cout << A[i] << " ";
  }
  std::cout << "\n";

  std::cout << "B:\n";
  for (int i = 0; i < size * size; i++) {
      if(i % size == 0){
          std::cout << "\n";
      }
      std::cout << B[i] << " ";
  }
  std::cout << "\n";

  std::cout << "C:\n";
  for (int i = 0; i < size * size; i++) {
      if(i % size == 0){
          std::cout << "\n";
      }
      std::cout << C[i] << " ";
  }
  std::cout << "\n";
}

void matrix_multiply(double* A, double* B, double* C, int size){
  #pragma omp parallel for collapse(2)
  for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
          double sum = 0.0;
          for (int k = 0; k < size; k++) {
              sum += A[i*size + k] * B[k*size + j];
          }
          C[i*size + j] += sum;
      }
  }
}

extern "C" void dgemm_(const char *transa, const char *transb, const int *m, const int *n, const int *k, const double *alpha, const double *a, const int *lda, const double *b, const int *ldb, const double *beta, double *c, const int *ldc);

int main(int argc, char* argv[])
{
  /**********************************************************
  *  Initial Setup
  **********************************************************/
  size_t N = 1024;
  double one = 1.0;
  double *A,*B,*C, *tmpA, *tmpB;
  MPI_Request request[4];

  int rank, size;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

 /**********************************************************
  *  Determining rank geometry < You need to improve this >
  **********************************************************/

  int p = sqrt(size);
  if (size != p*p) { printf("[Error] Number of processors must be a square integer.\n"); MPI_Abort(MPI_COMM_WORLD, -1); }

  int coords[2] = {0, 0};
  int nums[2]     = {p, p};
  int periodic[2] = {true, true};

  MPI_Comm cannonComm;
  MPI_Cart_create(MPI_COMM_WORLD, 2, nums, periodic, true, &cannonComm);

  int up, down, left, right;
  MPI_Cart_shift(cannonComm, 1, 1, &left, &right);
  MPI_Cart_shift(cannonComm, 0, 1, &up, &down);

  MPI_Cart_coords(cannonComm, rank, 2, coords);
  int rankx = coords[0];
  int ranky = coords[1];

  #pragma omp parallel
  {
    #pragma omp critical
    std::cout << "Hello! I am MPI rank[" << rank << "] with: " << rankx << ranky << "," << up << down << left << right << " omp thread[" << omp_get_thread_num() << "]\n"; 
  }
  MPI_Barrier(MPI_COMM_WORLD);


 /********************************************
  *   Initializing Matrices
  *******************************************/

  // Determining local matrix side size (n)
  const int n = N/p;

  // Allocating space for local matrices A, B, C
  A    = (double*) calloc (n*n, sizeof(double));
  B    = (double*) calloc (n*n, sizeof(double));
  C    = (double*) calloc (n*n, sizeof(double));



  // Allocating space for recieve buffers for A and B local matrices
  tmpA = (double*) calloc (n*n, sizeof(double));
  tmpB = (double*) calloc (n*n, sizeof(double));

  // Initializing values for input matrices A and B


  double * A_gl = (double*) calloc (N*N, sizeof(double));
  double * B_gl = (double*) calloc (N*N, sizeof(double));
  double * C_gl = (double*) calloc (N*N, sizeof(double));
  for(int i = 0; i < N*N; ++i){
      A_gl[i] = 1.0/i;
  }
  for(int i = 0; i < N; ++i){
      for(int j = 0; j < N; ++j){
          B_gl[j*N + i] = 1.0/(i+j);
      }
  }

  // Control solution in C_gl
  matrix_multiply(A_gl, B_gl, C_gl, N);


  if(rank == 0 && N < 9){
    print_matrices(A_gl, B_gl, C_gl, N);
  }
  MPI_Barrier(MPI_COMM_WORLD);
  
  // Initialize matrix A & B accordingly in Cannon algorithm style wiki: https://en.wikipedia.org/wiki/Cannon%27s_algorithm
  for(int i = 0; i < n; ++i){
    for(int j = 0; j < n; ++j){
      int k = (rankx + ranky) % p;
      A[i*n + j] = A_gl[i*N + j + rankx*n*N + k*n];
      B[i*n + j] = B_gl[i*N + j + k*n*N + ranky*n];
    }
  }

  /*
  if(rank == 1 && n < 5){
    print_matrices(A, B, C, n);
  }
  MPI_Barrier(MPI_COMM_WORLD);
  */
  
  
 /*******************************************************
  *  Creating Contiguous Datatype
  ******************************************************/

  MPI_Datatype subMatrixType;
  MPI_Type_contiguous(n*n, MPI_DOUBLE, &subMatrixType);
  MPI_Type_commit(&subMatrixType);

 /**************************************************************
  *   Running Cannon's Matrix-Matrix  Multiplication Algorithm
  **************************************************************/

  if (rank == 0) printf("Running Matrix-Matrix Multiplication...\n");

  // Registering initial time. It's important to precede this with a barrier to make sure
  // all ranks are ready to start when we take the time.
  MPI_Barrier(MPI_COMM_WORLD);
  double execTime = -MPI_Wtime();

  //matrix_multiply(A, B, C, n);

  for(int step = 0; step < p; step++)
  {
    MPI_Irecv(tmpA, 1, subMatrixType, right, 0, cannonComm, &request[0]);
    MPI_Irecv(tmpB, 1, subMatrixType, down, 1, cannonComm, &request[1]);

    MPI_Isend(A, 1, subMatrixType, left, 0, cannonComm, &request[2]);
    MPI_Isend(B, 1, subMatrixType, up, 1, cannonComm, &request[3]);

    matrix_multiply(A, B, C, n);
    MPI_Waitall(4, request, MPI_STATUS_IGNORE);

    double* holdA= A; A = tmpA; tmpA = holdA;
    double* holdB= B; B = tmpB; tmpB = holdB;


  }

  // Registering final time. It's important to precede this with a barrier to make sure all ranks have finished before we take the time.
  MPI_Barrier(MPI_COMM_WORLD);
  execTime += MPI_Wtime();

 /**************************************************************
  *   Verification Stage
  **************************************************************/

  /*
  if(rank == 3 && n < 5){
    print_matrices(tmpA, tmpB, C, n);
  }
  MPI_Barrier(MPI_COMM_WORLD);
  */

  verifyMatMulResults(C_gl, C, n, N, rankx, ranky, execTime);
  free(A); free(B); free(C); free(tmpA); free(tmpB);
  MPI_Comm_free(&cannonComm);
  return MPI_Finalize();
}
