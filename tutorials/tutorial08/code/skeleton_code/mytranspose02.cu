#include <cuda_runtime.h>
// coalesced transpose
// Uses shared memory to achieve coalesing in both reads and writes
// Tile width == #banks causes shared memory bank conflicts.

#define TILE_DIM 32
#define BLOCK_ROWS 8
__global__ void myCopyKernel(float *odata, const float *idata)
{
  __shared__ float tile[TILE_DIM][TILE_DIM];
    
  int x = blockIdx.x * TILE_DIM + threadIdx.x;
  int y = blockIdx.y * TILE_DIM + threadIdx.y;
  int width = gridDim.x * TILE_DIM;

  for (int j = 0; j < TILE_DIM; j += BLOCK_ROWS){
    //TODO: copy data from idata to tile
  }

  __syncthreads();

  // transpose block offset
  // x = TODO
  // y = TODO

  for (int j = 0; j < TILE_DIM; j += BLOCK_ROWS){
    // TODO: copy data from tile to odata
  }
}


__host__ void myKernelInfo(int* tile_dim, int* block_rows, int* transpose)
{
  *tile_dim = TILE_DIM;
  *block_rows = BLOCK_ROWS;
  *transpose =1;
}
