#include <cuda_runtime.h>
// naive transpose
// Simplest transpose; doesn't use shared memory.
// Global memory reads are coalesced but writes are not.


#define TILE_DIM 32
#define BLOCK_ROWS 8
__global__ void myCopyKernel(float *odata, const float *idata)
{
  int x = blockIdx.x * TILE_DIM + threadIdx.x;
  int y = blockIdx.y * TILE_DIM + threadIdx.y;
  int width = gridDim.x * TILE_DIM;

  for (int j = 0; j < TILE_DIM; j+= BLOCK_ROWS)
    odata[x*width + (y+j)] = idata[(y+j)*width + x];
}


__host__ void myKernelInfo(int* tile_dim, int* block_rows, int* transpose)
{
  *tile_dim = TILE_DIM;
  *block_rows = BLOCK_ROWS;
  *transpose =1;
}
