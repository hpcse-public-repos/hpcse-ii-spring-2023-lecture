#include <cuda_runtime.h>
// simple copy kernel
// Copy kernel using shared memory

#define TILE_DIM 32
#define BLOCK_ROWS 8
__global__ void myCopyKernel(float *odata, const float *idata)
{
  // TODO: declare shared memory `tile`
  
  int x = blockIdx.x * TILE_DIM + threadIdx.x;
  int y = blockIdx.y * TILE_DIM + threadIdx.y;
  int width = gridDim.x * TILE_DIM;

  for (int j = 0; j < TILE_DIM; j += BLOCK_ROWS){
    //TODO: copy data from global memory to shared memory
  }

  __syncthreads();

  for (int j = 0; j < TILE_DIM; j += BLOCK_ROWS){
    //TODO: copy data from shared memory to global memory
  }
}


__host__ void myKernelInfo(int* tile_dim, int* block_rows, int* transpose)
{
  *tile_dim = TILE_DIM;
  *block_rows = BLOCK_ROWS;
  *transpose =0;
}
