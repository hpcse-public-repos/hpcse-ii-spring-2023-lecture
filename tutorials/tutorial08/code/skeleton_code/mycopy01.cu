#include <cuda_runtime.h>
// simple copy kernel
// Used as reference case representing best effective bandwidth.

#define TILE_DIM 32
#define BLOCK_ROWS 8
__global__ void myCopyKernel(float *odata, const float *idata)
{
  int x = blockIdx.x * TILE_DIM + threadIdx.x;
  int y = blockIdx.y * TILE_DIM + threadIdx.y;
  int width = gridDim.x * TILE_DIM;

  for (int j = 0; j < TILE_DIM; j+= BLOCK_ROWS){
    //TODO: copy data from idata to odata
  }
}


__host__ void myKernelInfo(int* tile_dim, int* block_rows, int* transpose)
{
  *tile_dim = TILE_DIM;
  *block_rows = BLOCK_ROWS;
  *transpose =0;
}
