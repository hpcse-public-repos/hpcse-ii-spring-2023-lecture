#include <cuda_runtime.h>
// No bank-conflict transpose
// Same as transposeCoalesced except the first tile dimension is padded 
// to avoid shared memory bank conflicts.

#define TILE_DIM 32
#define BLOCK_ROWS 8
__global__ void myCopyKernel(float *odata, const float *idata)
{

  //TODO: modify the following line, decalre shared memory, avoid bank conflicts
  __shared__ float tile[1][1];

  int x = blockIdx.x * TILE_DIM + threadIdx.x;
  int y = blockIdx.y * TILE_DIM + threadIdx.y;
  int width = gridDim.x * TILE_DIM;

  for (int j = 0; j < TILE_DIM; j += BLOCK_ROWS)
     tile[threadIdx.y+j][threadIdx.x] = idata[(y+j)*width + x];

  __syncthreads();

  x = blockIdx.y * TILE_DIM + threadIdx.x;  // transpose block offset
  y = blockIdx.x * TILE_DIM + threadIdx.y;

  for (int j = 0; j < TILE_DIM; j += BLOCK_ROWS)
     odata[(y+j)*width + x] = tile[threadIdx.x][threadIdx.y + j];
}


__host__ void myKernelInfo(int* tile_dim, int* block_rows, int* transpose)
{
  *tile_dim = TILE_DIM;
  *block_rows = BLOCK_ROWS;
  *transpose =1;
}
