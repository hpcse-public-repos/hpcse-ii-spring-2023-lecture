/* Copyright (c) 1993-2015, NVIDIA CORPORATION. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <stdio.h>

__global__ void staticReverse(double *d)
{
  __shared__ double s[64];
  int t = threadIdx.x;
  int tr = 64-t-1;
  s[t] = d[t];
  __syncthreads();
  d[t] = s[tr];
}

__global__ void dynamicReverse(int n, int* i, double* d)
{
  extern __shared__ char s[];
  int*    si = (int*)&s[0];
  double* sd = (double*)&si[n];
  int t = threadIdx.x;
  int tr = n-t-1;
  si[t] = i[t];
  sd[t] = d[t];
  __syncthreads();
  i[t] = si[tr];
  d[t] = sd[tr];
}

int main(void)
{
  const int n = 64;
  int a[n], r[n], i[n];
  double b[n], d[n];
  
  for (int k = 0; k < n; k++) {
    a[k] = k;
    b[k] = (double)k;
    r[k] = n-k-1;
    i[k] = 0;
    d[k] = 0.;
  }

  int*    d_i;
  double* d_d;
  cudaMalloc(&d_i, n * sizeof(int)); 
  cudaMalloc(&d_d, n * sizeof(double)); 
  
  // run version with static shared memory
  cudaMemcpy(d_d, b, n*sizeof(double), cudaMemcpyHostToDevice);
  staticReverse<<<1,n>>>(d_d);
  cudaDeviceSynchronize();
  cudaMemcpy(d, d_d, n*sizeof(double), cudaMemcpyDeviceToHost);
  for (int k = 0; k < n; k++) 
    if (d[k] != (double)r[k]) printf("Error: d[%d]!=r[%d] (%f, %d)\n", k, k, d[k], r[k]);
  
  // run dynamic shared memory version
  cudaMemcpy(d_i, a, n*sizeof(int), cudaMemcpyHostToDevice);
  cudaMemcpy(d_d, b, n*sizeof(double), cudaMemcpyHostToDevice);
  dynamicReverse<<<1,n,n*sizeof(int) + n*sizeof(double)>>>(n, d_i, d_d);
  cudaDeviceSynchronize();
  cudaMemcpy(i, d_i, n * sizeof(int), cudaMemcpyDeviceToHost);
  cudaMemcpy(d, d_d, n * sizeof(double), cudaMemcpyDeviceToHost);
  for (int k = 0; k < n; k++) 
    if (i[k] != r[k]) printf("Error: d[%d]!=r[%d] (%d, %d)\n", k, k, i[k], r[k]);
  for (int k = 0; k < n; k++) 
    if (d[k] != (double)r[k]) printf("Error: d[%d]!=r[%d] (%f, %d)\n", k, k, d[k], r[k]);

  cudaFree(d_i);
  cudaFree(d_d);
}
