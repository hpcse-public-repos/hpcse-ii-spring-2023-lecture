# HPCSE II - Spring 2023 Lecture

Public repository to upload HW codes and lecture notes for HPCSE II, 2023.

Lecture / Exercise zoom link: https://ethz.zoom.us/j/67466579570, Password:hpcii2023
