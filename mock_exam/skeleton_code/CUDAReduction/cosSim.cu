#include "utils.h"
#include <cassert>


// TODO: Implement any __global__ or __device__ functions you need.



/// Compute cosine similarity between arrays aDev and bDev of size N and store
/// the result at *cosSimDev. aDev, bDev and cosSimDev are all device pointers.
void cosSim1M(const double *aDev, const double *bDev, double *cosSimDev, int N) {
    assert(N <= 1024 * 1024);

    // TODO: Implement any required memory allocations, deallocations and kernel launches.




}




#include "_cosSim.h"

int main() {
    testCosSim("cosSim1M", 1024);
    testCosSim("cosSim1M", 12341);
    testCosSim("cosSim1M", 1012311);
    printf("cosSim OK!\n");
}
