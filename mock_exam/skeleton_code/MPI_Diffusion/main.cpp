#include <cassert>
#include <iostream>
#include <cmath>
#include <omp.h>
#include <mpi.h>
#include <vector>

struct Equation2D
{
  /*
   * Solve the diffusion equation: du/dt = d^2u/dx^2 + d^2u/dy^2
   * in the [0,1]x[0,1] square with zero boundary conditions.
   */

  const double L{1.0};  // size of the square domain (0<x<L, 0<y<L)
  int Ntot;             // grid points per direction (global quantity)
  int N;                // grid points per direction for this rank
  int N_halo;           // = N + 2
  double h;             // grid spacing (dx = dy = h)
  double dt;            // timestep

  double * u_old;       // solution vector at timestep n
  double * u;           // solution vector at timestep n+1 (and Jacobi iteration m)
  double * u_new;       // solution vector at timestep n+1 (and Jacobi iteration m+1)

  MPI_Comm cart_comm;              // cartesian topology communicator
  int size;                        // total number of MPI ranks
  int rank;                        // ID of this rank in the cartesian communicator
  int rank_plus [2];               // Neighboring ranks in Cartesian grid
  int rank_minus[2];               // Neighboring ranks in Cartesian grid
  int coords[2]={0};               // Indices (I,J) of this rank's location in the Cartesian grid
  double origin[2]={0};            // Indices (I,J) converted to location in space
  MPI_Datatype SEND_HALO_PLUS [2]; // MPI datatype to send halo cells
  MPI_Datatype SEND_HALO_MINUS[2]; // MPI datatype to send halo cells
  MPI_Datatype RECV_HALO_PLUS [2]; // MPI datatype to receive halo cells
  MPI_Datatype RECV_HALO_MINUS[2]; // MPI datatype to receive halo cells
  MPI_Request request[8];

  double uIntegral;  // Q(t) = integral {u(x,y,t) dx dy}
  double error_norm; // Jacobi iterations error (Em)
  double const1;     // auxiliary constant used in Jacobi iterations
  double const2;     // auxiliary constant used in Jacobi iterations


  Equation2D(const int a_N, const MPI_Comm comm) : Ntot(a_N)
  {
    MPI_Comm_size(comm, &size);
    const int ranks_per_dim = sqrt(size);
    assert (size == ranks_per_dim * ranks_per_dim && "Number of processes must be a square number.\n");

    N      = Ntot / ranks_per_dim; //grid points per dimension for each rank
    N_halo = N + 2;                //add two points for halo cells
    h      = L / (Ntot);           //grid spacing

    dt = 0.1;
    const1 = h*h/dt;
    const2 = 1/(const1+4.0);

    //Allocation of arrays
    u     = new double[N_halo * N_halo];
    u_old = new double[N_halo * N_halo];
    u_new = new double[N_halo * N_halo];

    //QUESTION A part 1 of 2- START//
    ////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    
    
    
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    //QUESTION A part 1 of 2 - END  //

    //Convert indices to location in space
    origin[0] = N * h * coords[0];
    origin[1] = N * h * coords[1];



    //QUESTION B part 1 of 2 - START//
    //Define custom datatypes to send/receive an Nx1 or an 1xN array of halo cells at each boundary
    ////////////////////////////////////////////////////////////////////////////////////////////////
































    ////////////////////////////////////////////////////////////////////////////////////////////////
    //QUESTION B part 1 of 2 - END  //
  }

  //do not modify this function
  double initialCondition(const double x, const double y) const
  {
    if (x<0 || x>1 || y<0 || y>1) return 0;
    return sin(M_PI*x)*sin(M_PI*y);
  }

  //do not modify this function
  void applyStencil(const int i, const int j)
  {
    const int center = i * N_halo + j;
    u_new[center] = const2 * ( const1 * u_old[center] + u[(i+1)*N_halo+j] + u[(i-1)*N_halo+j] + u[i*N_halo+(j+1)] + u[i*N_halo+(j-1)]);
  }

  void ComputeIntegral()
  {
    //QUESTION D part 1 of 2- START//
    //modify the following to have a reduction of Q(t) among MPI ranks
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #pragma omp single
    {
      uIntegral = 0;
    }
    #pragma omp for collapse (2) reduction(+:uIntegral)
    for (int i = 1; i < N + 1; ++i)
    for (int j = 1; j < N + 1; ++j)
    {
      uIntegral += u[i * N_halo + j];
    }
    #pragma omp single
    {
      uIntegral *= h*h;
    }
    const int tid = omp_get_thread_num();
    if (tid == 0)
      std::cout << " --> Q(t) = "  << uIntegral << std::endl;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    //QUESTION D part 1 of 2 - END  //
  }

  double JacobiStep()
  {
    //QUESTION C - START//
    //modify the following to enable MPI communication and communication & computation overlap
    ////////////////////////////////////////////////////////////////////////////////////////////////

    #pragma omp for collapse(2) //nowait
    for (int i = 1; i < N + 1; ++i)
    for (int j = 1; j < N + 1; ++j)
    {
      applyStencil(i, j);
    }


































    ////////////////////////////////////////////////////////////////////////////////////////////////
    //QUESTION C - END  //

    #pragma omp single
    {
      std::swap(u_new,u);
    }

    //QUESTION D part 2 of 2  - START//
    //modify the following to have a reduction of Q(t) among MPI ranks
    ////////////////////////////////////////////////////////////////////////////////////////////////
    #pragma omp single
    {
      error_norm = 0;
    }
    #pragma omp for collapse (2) reduction(+:error_norm)
    for (int i = 1; i < N + 1; ++i)
    for (int j = 1; j < N + 1; ++j)
    {
      error_norm += std::fabs(u[i * N_halo + j]-u_new[i * N_halo + j]);
    }    
    #pragma omp single
    {
      error_norm *= h*h;
    }
    return error_norm;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    //QUESTION D part 2 of 2 - END  //
  }


  //do not modify this function
  void run(const double t_end)
  {
    const double epsilon = 1e-6; //tolerance for Jacobi iterations
    #pragma omp parallel
    {
      //Set initial condition
      #pragma omp for collapse (2) 
      for (int i = -1; i < N+1; ++i)
      for (int j = -1; j < N+1; ++j)
      {
        const int idx = (i + 1) * N_halo + (j + 1);
        const double x = origin[0] + i * h + 0.5*h;
        const double y = origin[1] + j * h + 0.5*h;
        u    [idx] = initialCondition(x, y);
        u_old[idx] = u[idx];
        u_new[idx] = u[idx];
      }
  
      double t = 0.0;
      while (t < t_end)
      {
        ComputeIntegral(); //compute Q(t) = int_{0}^{1}int_{0}^{1} u(x,y,t) dx dy
  
        for (int m = 0 ; m < 5 ; m ++) //perform Jacobi iterations (up to 1000)
        {
          if (JacobiStep() < epsilon) break;
        }

        // Swap vectors
        #pragma omp single
        {
          std::swap(u_old,u);
          std::swap(u_new,u);
        }
  
        t += dt;
      }//while (t < t_end)
    }//pragma omp parallel
  }


  ~Equation2D()
  {
    delete [] u;
    delete [] u_old;
    delete [] u_new;
    //QUESTIONS A and B part 2 of 2- START//
    //free datatypes and communicator
    ////////////////////////////////////////////////////////////////////////////////////////////////









    ////////////////////////////////////////////////////////////////////////////////////////////////
    //QUESTIONS A and B part 2 of 2- END  //
  }
};

//do not modify this function
int main(int argc, char **argv)
{
  int threadSafety;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &threadSafety);

  {
    const int gridpoints_per_dim = 128;
    const double t_end           = 10;
    Equation2D simulation = Equation2D(gridpoints_per_dim, MPI_COMM_WORLD);
    simulation.run(t_end);
  }

  MPI_Finalize();

  return 0;
}
