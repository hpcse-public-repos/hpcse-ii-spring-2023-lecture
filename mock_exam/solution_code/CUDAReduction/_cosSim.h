#pragma once

#include "utils.h"
#include <algorithm>
#include <cstdio>
#include <random>

/// Test large reductions (up to 1024^3 and larger).
inline void testCosSim(const char *name, int N) {
    double *aHost;
    double *bHost;
    double *aDev;
    double *bDev;
    double *cosSimDev;

    CUDA_CHECK(cudaMallocHost(&aHost, N * sizeof(double)));
    CUDA_CHECK(cudaMallocHost(&bHost, N * sizeof(double)));
    CUDA_CHECK(cudaMalloc(&aDev, N * sizeof(double)));
    CUDA_CHECK(cudaMalloc(&bDev, N * sizeof(double)));
    CUDA_CHECK(cudaMalloc(&cosSimDev, 1 * sizeof(double)));


    std::mt19937 gen;
    std::uniform_real_distribution<double> unif(-10., 10.);
    for (int i = 0; i < N; ++i)
    {
        aHost[i] = unif(gen);
        bHost[i] = unif(gen);
    }
    std::shuffle(aHost, aHost + N, gen);
    std::shuffle(bHost, bHost + N, gen);
    CUDA_CHECK(cudaMemcpy(aDev, aHost, N * sizeof(double), cudaMemcpyHostToDevice));
    CUDA_CHECK(cudaMemcpy(bDev, bHost, N * sizeof(double), cudaMemcpyHostToDevice));

    cosSim1M(aDev, bDev, cosSimDev, N);

    auto Ddot = [](double* a, double* b, int N) -> double {
      double ddot = 0.;
      for (int i(0); i < N; i++)
        ddot += a[i]*b[i];
      return ddot;
    };
    double expected = Ddot(aHost, bHost, N) / (std::sqrt(Ddot(aHost, aHost, N) * Ddot(bHost, bHost, N)));
    double received;
    CUDA_CHECK(cudaMemcpy(&received, cosSimDev, 1 * sizeof(double), cudaMemcpyDeviceToHost));
    if (std::abs(expected - received) > 1e-10) {
        printf("%s incorrect result:  N=%d  expected=%f  received=%f\n",
               name, N, expected, received);
        exit(1);
    }

    CUDA_CHECK(cudaFree(aDev));
    CUDA_CHECK(cudaFree(bDev));
    CUDA_CHECK(cudaFree(cosSimDev));
    CUDA_CHECK(cudaFreeHost(aHost));
    CUDA_CHECK(cudaFreeHost(bHost));
}
