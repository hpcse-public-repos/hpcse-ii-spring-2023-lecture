#include "utils.h"
#include <cassert>

// Define shfl_down_sync for custom datatype
__device__ double3 shfl_down_sync(unsigned mask, double3 t, unsigned delta) {
    return {
        __shfl_down_sync(mask, t.x, delta),
        __shfl_down_sync(mask, t.y, delta),
        __shfl_down_sync(mask, t.z, delta)
    };
}

__device__ double3 add3(double3 a, double3 b)
{
  a.x += b.x; 
  a.y += b.y; 
  a.z += b.z; 
  return a;
}

__device__ double3 sumWarp(double3 t) {
    t = add3(t, shfl_down_sync(0xFFFFFFFF, t, 1));
    t = add3(t, shfl_down_sync(0xFFFFFFFF, t, 2));
    t = add3(t, shfl_down_sync(0xFFFFFFFF, t, 4));
    t = add3(t, shfl_down_sync(0xFFFFFFFF, t, 8));
    t = add3(t, shfl_down_sync(0xFFFFFFFF, t, 16));
    return t;
}

/// Returns the sum of all values `a` within a block,
/// with the correct answer returned by the 0th thread of a block.
__device__ double3 sumBlock(double3 a) {

    // Reduce triplet within warps
    double3 partial = sumWarp(a);

    // Write result to shared memory, launch config must satisfy blockDim.x / warpSize == 32
    __shared__ double3 partials[32];
    int warpIdx = threadIdx.x / warpSize;
    if (threadIdx.x % warpSize == 0)
        partials[warpIdx] = partial;
    __syncthreads();

    // Reduce triplet within block
    if (warpIdx == 0)
        return sumWarp(partials[threadIdx.x]);

    return double3{0., 0., 0.};
}

__global__ void cosSimKernel1(const double *a, const double *b, double3 *tmp, int N) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    double3 t = idx < N ? double3{a[idx]*a[idx], b[idx]*b[idx], a[idx]*b[idx]} : double3{0., 0., 0.};
    double3 result = sumBlock(t);
    if (threadIdx.x == 0)
        tmp[blockIdx.x] = result;
}

__global__ void cosSimKernel2(const double3 *tmp, double *cosSim, int numBlocks) {
    int idx = threadIdx.x;
    double3 partial = idx < numBlocks ? tmp[idx] : double3{0., 0., 0.};
    double3 result = sumBlock(partial);
    if (threadIdx.x == 0)
        *cosSim = result.z * rsqrt(result.x * result.y);
}

/// Compute cosine similarity between arrays aDev and bDev of size N and store
/// the result at *cosSimDev. aDev, bDev and cosSimDev are all device pointers.
void cosSim1M(const double *aDev, const double *bDev, double *cosSimDev, int N) {
    assert(N <= 1024 * 1024);
    int blocks = (N + 1023) / 1024;
    double3 *tmpDev;
    CUDA_CHECK(cudaMalloc(&tmpDev, blocks * sizeof(double3)));

    // Pass 1
    cosSimKernel1<<<blocks, 1024>>>(aDev, bDev, tmpDev, N);
    CUDA_CHECK(cudaGetLastError());
    // Pass 2
    cosSimKernel2<<<1, 1024>>>(tmpDev, cosSimDev, blocks);
    CUDA_CHECK(cudaGetLastError());

    CUDA_CHECK(cudaFree(tmpDev));
}

#include "_cosSim.h"

int main() {
    testCosSim("cosSim1M", 1024);
    testCosSim("cosSim1M", 12341);
    testCosSim("cosSim1M", 1012311);
    printf("cosSim OK!\n");
}
