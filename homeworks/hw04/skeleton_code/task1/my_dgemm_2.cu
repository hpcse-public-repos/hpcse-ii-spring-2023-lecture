#include <cuda_runtime.h>
#include "utils.h"

#define BLOCK_SIZE 16

__global__ void sharedDgemm(
    const int m,
    const int n,
    const int k,
    const double alpha,
    const double* const A,
    const double* const B,
    const double beta,
    double* const C)
{
//   TODO
}

void myDgemm(
    const int m,
    const int n,
    const int k,
    const double alpha,
    const double* const A,
    const double* const B,
    const double beta,
    double* const C)
{
//  TODO
//  dim3 dimGrid(...);
//  dim3 dimBlock(...);

  sharedDgemm<<<1, 1>>>(m, n, k, alpha, A, B, beta, C);
  CUDA_CHECK(cudaGetLastError());
}
