#include <cuda_runtime.h>
#include "utils.h"

// Each thread carries out accumulation on 4x4 tile
#define THREAD_TILE   4
#define BLOCK_TILE    8
#define BLOCK_ITEMS_XY (THREAD_TILE * BLOCK_TILE)

// sharedDgemmC<<<dim3(m/BLOCK_ITEMS_XY, n/BLOCK_ITEMS_XY), dim3(BLOCK_TILE, BLOCK_TILE)>>>(args...)
__global__ void sharedDgemm(
    const int m,
    const int n,
    const int k,
    const double alpha,
    const double* const A,
    const double* const B,
    const double beta,
    double* const C)
{
    // Static declaration of shared memory to store Asub and Bsub respectively
    __shared__ double As[BLOCK_ITEMS_XY][BLOCK_ITEMS_XY]; // [col][row]
    __shared__ double Bs[BLOCK_ITEMS_XY][BLOCK_ITEMS_XY];

    // Register array declaration to store accumulation result
    double Cvalue[THREAD_TILE][THREAD_TILE];
    for (int v=0; v < THREAD_TILE; ++v)
    for (int u=0; u < THREAD_TILE; ++u)
        Cvalue[v][u] = 0.;

    // Register arrays for fragments of A and b
    double Af[THREAD_TILE];
    double Bf[THREAD_TILE];

    // Block row and column (BLOCK_{x,y}, see Figure 2)
    const int blockRow = blockIdx.x * BLOCK_ITEMS_XY;
    const int blockCol = blockIdx.y * BLOCK_ITEMS_XY;

    const int threadRow = threadIdx.x * THREAD_TILE;
    const int threadCol = threadIdx.y * THREAD_TILE;

    // Loop over all the sub-matrices of A and B
    for (int i = 0; i < (k / BLOCK_ITEMS_XY); ++i)
    {
        // Load Asub and Bsub from device memory to shared memory
        const int blockStride = i*BLOCK_ITEMS_XY;
        // (BLOCK_TILE * BLOCK_TILE) appear THREAD_TILE * THREAD_TILE times
        // in shared memory
        for (int v=0; v < THREAD_TILE; v++)
        for (int u=0; u < THREAD_TILE; u++)
        {
          const int sharedRow = u*BLOCK_TILE + threadIdx.x;
          const int sharedCol = v*BLOCK_TILE + threadIdx.y;
          As[sharedCol][sharedRow] = A[(blockStride + sharedCol)*m + blockRow + sharedRow];
          Bs[sharedCol][sharedRow] = B[(blockCol + sharedCol)*k + blockStride+sharedRow];
        }

        // Synchronize to make sure the sub-matrices are loaded
        // before starting the computation
        __syncthreads();
        for (int j=0; j < BLOCK_ITEMS_XY; ++j)
        {
          for (int t=0; t < THREAD_TILE; ++t)
          {
            Af[t] = As[j][threadRow+t];
            Bf[t] = Bs[threadCol+t][j];
          }
          for (int u=0; u < THREAD_TILE; u++)
          for (int v=0; v < THREAD_TILE; v++)
            Cvalue[u][v] += Af[v]*Bf[u];
        }

        // Synchronize to make sure that the preceding computation is done before loading
        // two new sub-matrices of A and B in the next iteration
        __syncthreads();
    }

    // Write Csub to device memory
    for (int v=0; v < THREAD_TILE; ++v)
    for (int u=0; u < THREAD_TILE; ++u)
    {
      const int linIdx = (blockCol + threadCol + v)*m + blockRow + threadRow + u;
      C[linIdx] = alpha*Cvalue[v][u] + beta*C[linIdx];
    }
}

void myDgemm(
    const int m,
    const int n,
    const int k,
    const double alpha,
    const double* const A,
    const double* const B,
    const double beta,
    double* const C)
{
  // Note that xy coordinate of launch config swapped wrt to shared_dgemm_1.cu
  dim3 dimGrid(m/BLOCK_ITEMS_XY, n/BLOCK_ITEMS_XY);
  dim3 dimBlock(BLOCK_TILE, BLOCK_TILE);

  sharedDgemm<<<dimGrid, dimBlock>>>(m, n, k, alpha, A, B, beta, C);
  CUDA_CHECK(cudaGetLastError());
}
