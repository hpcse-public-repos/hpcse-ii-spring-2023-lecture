#include <cuda_runtime.h>
#include "utils.h"

// Each thread carries out accumulation on four 4x4 tiles
#define HALF_TILE      4
#define THREAD_TILE    (2 * HALF_TILE)
#define BLOCK_ITEMS_X  128
#define BLOCK_ITEMS_Y  64
#define BLOCK_ITEMS_K  16
#define NUM_THREADS    (BLOCK_ITEMS_X * BLOCK_ITEMS_Y / (THREAD_TILE * THREAD_TILE))
#define STRIDE_A       (NUM_THREADS / BLOCK_ITEMS_X)
#define STRIDE_B       (NUM_THREADS / BLOCK_ITEMS_K)
#define SCPY_ITERS_A   (BLOCK_ITEMS_X * BLOCK_ITEMS_K / NUM_THREADS)
#define SCPY_ITERS_B   (BLOCK_ITEMS_Y * BLOCK_ITEMS_K / NUM_THREADS)

// sharedDgemmC<<<dim3(m/BLOCK_ITEMS_XY, n/BLOCK_ITEMS_XY), dim3(DIM_BLOCK_XY, DIM_BLOCK_XY)>>>(args...)
__global__ void sharedDgemm(
    const int m,
    const int n,
    const int k,
    const double alpha,
    const double* __restrict__ const A,
    const double* __restrict__ const B,
    const double beta,
    double* __restrict__ const C)
{
    // Static declaration of shared memory to store Asub and Bsub respectively
    __shared__ double As[BLOCK_ITEMS_K][BLOCK_ITEMS_X]; // [col][row]
    __shared__ double Bs[BLOCK_ITEMS_Y][BLOCK_ITEMS_K]; // [col][row]

    // Register array declaration to store accumulation result
    double Cvalue[THREAD_TILE][THREAD_TILE]; // [col][row]
    #pragma unroll
    for (int v=0; v < THREAD_TILE; ++v)
    #pragma unroll
    for (int u=0; u < THREAD_TILE; ++u)
        Cvalue[v][u] = 0.;

    // Register arrays for fragments of A and b
    double Af[HALF_TILE];
    double Bf[THREAD_TILE];

    // Block row and column offsets
    const int blockRow = blockIdx.x * BLOCK_ITEMS_X;
    const int blockCol = blockIdx.y * BLOCK_ITEMS_Y;

    const int threadRow = ((threadIdx.x / 64) * 8 + ((threadIdx.x % 16) / 2)) * HALF_TILE;
    const int threadCol = ((threadIdx.x % 2) + ((threadIdx.x % 64) / 16) * 2) * HALF_TILE;
    // Loop over all the sub-matrices of A and B
    for (int i = 0; i < (k / BLOCK_ITEMS_K); ++i)
    {
        // Load Asub and Bsub from device memory to shared memory
        const int blockStride = i*BLOCK_ITEMS_K;
        #pragma unroll
        for (int j=0; j < SCPY_ITERS_A; ++j)
        {
          const int rowA = threadIdx.x % BLOCK_ITEMS_X;
          const int colA = j*STRIDE_A + threadIdx.x / BLOCK_ITEMS_X;
          As[colA][rowA] = A[(blockStride + colA)*m + blockRow + rowA];
        }

        #pragma unroll
        for (int j=0; j < SCPY_ITERS_B; ++j)
        {
          const int rowB = threadIdx.x % BLOCK_ITEMS_K;
          const int colB = j*STRIDE_B + threadIdx.x / BLOCK_ITEMS_K;
          Bs[colB][rowB] = B[(blockCol + colB)*k + blockStride + rowB];
        }

        // Synchronize to make sure the sub-matrices are loaded
        // before starting the computation
        __syncthreads();

        for (int j=0; j < BLOCK_ITEMS_K; ++j)
        {
          #pragma unroll
          for (int t=0; t < HALF_TILE; ++t)
            Bf[t] = Bs[threadCol+t][j];
          #pragma unroll
          for (int t=0; t < HALF_TILE; ++t)
            Bf[HALF_TILE+t] = Bs[BLOCK_ITEMS_Y/2 + threadCol + t][j];

          #pragma unroll
          for (int u=0; u < HALF_TILE; ++u)
          {
            Af[u] = As[j][threadRow+u];
            #pragma unroll
            for (int v=0; v < HALF_TILE; ++v)
              Cvalue[v][u] += Af[u]*Bf[v];
          }

          #pragma unroll
          for (int v=HALF_TILE; v < THREAD_TILE; ++v)
          #pragma unroll
          for (int u=0; u < HALF_TILE; ++u)
            Cvalue[v][u] += Af[u]*Bf[v];

          #pragma unroll
          for (int u=0; u < HALF_TILE; ++u)
          {
            Af[u] = As[j][BLOCK_ITEMS_X/2 + threadRow+u];
            #pragma unroll
            for (int v=HALF_TILE; v < THREAD_TILE; ++v)
              Cvalue[v][HALF_TILE+u] += Af[u]*Bf[v];
          }

          #pragma unroll
          for (int v=0; v < HALF_TILE; ++v)
          #pragma unroll
          for (int u=0; u < HALF_TILE; ++u)
            Cvalue[v][HALF_TILE+u] += Af[u]*Bf[v];

        }

        // Synchronize to make sure that the preceding computation is done before loading
        // two new sub-matrices of A and B in the next iteration
        __syncthreads();
    }

    // Write back to C
    #pragma unroll
    for (int v=0; v < HALF_TILE; ++v)
    #pragma unroll
    for (int u=0; u < HALF_TILE; ++u)
    {
      const int linIdx = (blockCol + threadCol + v)*m + blockRow + threadRow + u;
      C[linIdx] = alpha*Cvalue[v][u] + beta*C[linIdx];
    }

    #pragma unroll
    for (int v=0; v < HALF_TILE; ++v)
    #pragma unroll
    for (int u=0; u < HALF_TILE; ++u)
    {
      const int linIdx = (blockCol + threadCol + v)*m + blockRow + threadRow + BLOCK_ITEMS_X/2 + u;
      C[linIdx] = alpha*Cvalue[v][HALF_TILE+u] + beta*C[linIdx];
    }

    #pragma unroll
    for (int v=0; v < HALF_TILE; ++v)
    #pragma unroll
    for (int u=0; u < HALF_TILE; ++u)
    {
      const int linIdx = (blockCol + threadCol + BLOCK_ITEMS_Y/2 + v)*m + blockRow + threadRow + BLOCK_ITEMS_X/2 + u;
      C[linIdx] = alpha*Cvalue[HALF_TILE+v][HALF_TILE+u] + beta*C[linIdx];
    }

    #pragma unroll
    for (int v=0; v < HALF_TILE; ++v)
    #pragma unroll
    for (int u=0; u < HALF_TILE; ++u)
    {
      const int linIdx = (blockCol + threadCol + BLOCK_ITEMS_Y/2 + v)*m + blockRow + threadRow + u;
      C[linIdx] = alpha*Cvalue[HALF_TILE+v][u] + beta*C[linIdx];
    }
}

void myDgemm(
    const int m,
    const int n,
    const int k,
    const double alpha,
    const double* const A,
    const double* const B,
    const double beta,
    double* const C)
{
  // Note that xy coordinate of launch config swapped wrt to shared_dgemm_1.cu
  dim3 dimGrid(m/BLOCK_ITEMS_X, n/BLOCK_ITEMS_Y);
  dim3 dimBlock(NUM_THREADS);

  sharedDgemm<<<dimGrid, dimBlock>>>(m, n, k, alpha, A, B, beta, C);
  CUDA_CHECK(cudaGetLastError());
}
