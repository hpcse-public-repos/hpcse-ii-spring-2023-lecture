#include <cuda_runtime.h>

__global__ void computeForcesKernel(int N, const double3 *p, double3 *f) {

    for(int idx = blockIdx.x * blockDim.x + threadIdx.x;
            idx < N;
            idx += gridDim.x * blockDim.x){

        double3 ftot = double3{0.0, 0.0, 0.0};
        double3 myP = p[idx];
        for (int i = 0; i < N; ++i) {
            double dx = p[i].x - myP.x;
            double dy = p[i].y - myP.y;
            double dz = p[i].z - myP.z;
            double r = sqrt(1e-150 + dx * dx + dy * dy + dz * dz);
            double inv_r = 1.0 / r;
            // replace repeated computation within one iteration
            double inv_rrr = inv_r * inv_r * inv_r;
            ftot.x += dx * inv_rrr;
            ftot.y += dy * inv_rrr;
            ftot.z += dz * inv_rrr;
        }
        f[idx] = ftot;
    }
}

void computeForces(int N, const double3 *p, double3 *f) {
    constexpr int numThreads = 1024;
    int numBlocks = (N + numThreads - 1) / numThreads;
    computeForcesKernel<<<numBlocks, numThreads>>>(N, p, f);
}
