#!/bin/bash -l

#SBATCH --nodes=2
#SBATCH --ntasks=2
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --output=slurm_output.txt
#SBATCH --error=slurm_error.txt
#SBATCH --partition=debug
#SBATCH --time=00:05:00

srun bw_bench
