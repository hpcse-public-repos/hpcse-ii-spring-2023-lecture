# To run your code on euler do this:

First run: `set_software_stack.sh new`

This will load you automatically to the new stack `env2lmod`. We are only work on this for this semester

Then logout and login and to only load the new stack

Then load:
`module load gcc/9.3.0 openmpi python`

Now you can compile and run your code on Euler