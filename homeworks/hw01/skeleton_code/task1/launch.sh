#!/bin/bash -l

#SBATCH --nodes=2
#SBATCH --ntasks=2
#SBATCH --ntasks-per-node=1
#SBATCH --output=slurm_output.txt
#SBATCH --error=slurm_error.txt
#SBATCH --time=00:05:00

mpirun ./bw_bench
