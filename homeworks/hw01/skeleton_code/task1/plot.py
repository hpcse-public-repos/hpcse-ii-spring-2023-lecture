#!/usr/bin/env python3
# File       : plot.py
# Created    : Wed Apr 28 2021 04:02:48 PM (+0200)
# Description: Plot script to visualize measured data
# Copyright 2021 ETH Zurich. All Rights Reserved.
import argparse
import numpy as np
import matplotlib.pyplot as plt

def parse_args(*, partial=False):
    parser = argparse.ArgumentParser()
    # yapf: disable
    parser.add_argument('-s', '--socket', nargs='+', type=str, help="Measurements on single socket", required=True)
    parser.add_argument('-n', '--node', nargs='+', type=str, help="Measurements on two nodes", required=True)
    # yapf: enable
    if partial:
        return parser.parse_known_args()
    else:
        return parser.parse_args()

def stats(files):
    d = np.loadtxt(files[0])
    ret = {'size': d[:, 0]}
    data = np.zeros((len(d), len(files)))
    for i, f in enumerate(files):
        d = np.loadtxt(f)
        data[:, i] = d[:, 1] / 1000
    ret['mean'] = np.mean(data, axis=1)
    ret['std'] = np.std(data, axis=1)
    return ret

if __name__ == "__main__":
    args = parse_args()
    s = stats(args.socket)
    n = stats(args.node)

    fig, ax = plt.subplots(figsize=(4, 3))
    ax.errorbar(s['size'],
                s['mean'],
                yerr=s['std'],
                label='OCN',
                linestyle='',
                marker='s')
    ax.errorbar(n['size'],
                n['mean'],
                yerr=n['std'],
                label='SAN',
                linestyle='',
                marker='^')
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.grid()

    ax.set_xlabel('Message size [byte]')
    ax.set_ylabel('Bandwidth [GB/s]')
    ax.legend()

    plt.show()
    fig.savefig('measurements.pdf', bbox_inches='tight')
