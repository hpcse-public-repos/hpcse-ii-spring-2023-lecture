#!/usr/bin/env python3

## In this example, we demonstrate how Korali finds values for the
## variables that maximize the objective function, given by a
## user-provided computational model.

# Importing computational model
import sys
import math
sys.path.append('./_model')
from model import *

# Import Korali TODO

# Starting Korali's Engine TODO

# Creating new experiment TODO

# Configuring Problem
e["Random Seed"] = 0xBEEF


# Defining the problem's variables. TODO

# Configuring CMA-ES parameters
e["Solver"]["Type"] = "Optimizer/CMAES"
e["Solver"]["Population Size"] = 32
e["Solver"]["Termination Criteria"]["Min Value Difference Threshold"] = 1e-14
e["Solver"]["Termination Criteria"]["Max Generations"] = 100

# Configuring results path TODO

# Running Korali TODO
