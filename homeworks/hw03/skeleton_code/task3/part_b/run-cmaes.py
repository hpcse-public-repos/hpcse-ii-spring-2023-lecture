#!/usr/bin/env python3

# In this example, we demonstrate how Korali find the variable values
# that maximize the posterior in a bayesian problem where the likelihood
# is calculated by providing reference data points and their objective values.


# Importing the computational model
import sys
sys.path.append('./_model')
from model import *

# Import Korali TODO

# Create the engine TODO

# Creating new experiment TODO

# Setting up the reference likelihood for the Bayesian Problem #TODO

# Configuring the problem's random distributions #TODO

# Configuring the problem's variables #TODO

# Configuring CMA-ES parameters
e["Solver"]["Type"] = "Optimizer/CMAES"
e["Solver"]["Population Size"] = 32
e["Solver"]["Termination Criteria"]["Min Value Difference Threshold"] = 1e-14
e["Solver"]["Termination Criteria"]["Max Generations"] = 500


# Configuring output settings #TODO

# Run the experiment #TODO
